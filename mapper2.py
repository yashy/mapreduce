#!/usr/bin/env python2.7
import sys
import re
import simplejson as json
from pprint import pprint
import string
from collections import defaultdict
import heapq

def main(argv):
	counter=0
	body={}
	bcount={}
	user={}
	ucount={}
	hashtag={}
	hcount={}
	try:
		for line in sys.stdin:
			#(word,value)=re.split('[ \t]', line)
			splitLine = line.strip().split()
			word = splitLine[0]
			value = splitLine[1]
			if word.startswith('@'):
				user[word]=int(value)
			elif word.startswith('#'):
				hashtag[word]=int(value)
			else:
				body[word]=int(value)		

		bcount=heapq.nlargest(10, body, key=body.get)	
		ucount=heapq.nlargest(10, user, key=user.get)
		hcount=heapq.nlargest(10, hashtag, key=hashtag.get)

		for i in bcount:
			print i +"\t"+str(body[i])
		for i in ucount:
			print i +"\t"+str(user[i])
		for i in hcount:
			print i +"\t"+str(hashtag[i])

	except "end of file":
		return None

if __name__ == "__main__":
     main(sys.argv)