#!/usr/bin/env python2.7
import sys
import re
import simplejson as json
from pprint import pprint
import string
from collections import defaultdict
from string import maketrans

def is_ascii(s):
    return all(ord(c) < 128 for c in s)

def main(argv):

	try:
		for line in sys.stdin:
		#print line
	#try
			try:
				data = json.loads(line)
			except:
				print "Invalid json"
		#pass

			text=data['text']
			s=text
			test = "[\.\t\,\:\;\(\)\"\!\.]"
			regex = re.compile('[%s]' % re.escape(test
				))
			text = regex.sub('', s)
			words = text.split()
			counts = defaultdict(list)

			for word, count in map(lambda x: (x, 1), words):
				counts[word].append(count)

		#print "Counts:", counts

    # Use the reduce() function to compute the total for each word.

			totals = {}
			user={}
			hashtags={}
			for word in counts:
				totals[word] = reduce(lambda x, y: x+y, counts[word])
				if is_ascii(word):
					print "LongValueSum:" + word + "\t" + "1"

	except "end of file":
		return None


if __name__ == "__main__":
     main(sys.argv)